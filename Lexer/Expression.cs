using System;

namespace Lexer
{
    /// <summary>
    ///   Represents a binary operation.
    /// </summary>
    internal class Expression : Operand
    {
        internal Operand LeftOperand;
        internal char Operator;
        internal Operand RightOperand;

        public Expression(string expression)
        {
            var operatorIndex = FindIndexOfLowestPriorityOperator(expression);

            // Get the operator
            Operator = Simplification.ParseOperator(expression.Substring(operatorIndex, 1));

            // Get the operands
            LeftOperand = ParseOperand(expression.Substring(0, operatorIndex));
            RightOperand = ParseOperand(expression.Substring(operatorIndex + 1));
        }

        /// <summary>
        ///   Given expression, finds the index of the operator which has the least precedence, ie the one which should be evaluated last.
        /// </summary>
        /// <param name = "expression"></param>
        /// <returns>0-based index of the lowest-precedence operator symbol.</returns>
        [Obsolete("Add real logic here.")]
        private static int FindIndexOfLowestPriorityOperator(string expression)
        {
            Console.WriteLine("What is the 0-based index of the lowest-priority operator in \"" + expression + "\"?");

            var operatorIndex = Int32.Parse(Console.ReadLine());
            return operatorIndex;
        }
    }
}