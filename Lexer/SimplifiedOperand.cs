namespace Lexer
{
    /// <summary>
    /// Represents an expression which can not be reduced any further.
    /// </summary>
    internal class SimplifiedOperand : Operand
    {
        public string Symbol;

        public SimplifiedOperand(string expression)
        {
            Symbol = expression;
        }
    }
}