using System;

namespace Lexer
{
    /// <summary>
    ///   Contains actual implementations of each of the supported operations.
    /// </summary>
    internal static class OperationImplementations
    {
        [Obsolete("Replace with call to actual Union function.")]
        public static SimplifiedOperand Union(SimplifiedOperand a, SimplifiedOperand b)
        {
            Console.WriteLine("What is the union of " + a.Symbol + " and " + b.Symbol + "?");
            var result = Console.ReadLine();
            return new SimplifiedOperand(result);
        }

        [Obsolete("Replace with call to actual Intersection function.")]
        public static SimplifiedOperand Intersection(SimplifiedOperand a, SimplifiedOperand b)
        {
            Console.WriteLine("What is the intersection of " + a.Symbol + " and " + b.Symbol + "?");
            var result = Console.ReadLine();
            return new SimplifiedOperand(result);
        }

        [Obsolete("Replace with call to actual SetDiff function.")]
        public static SimplifiedOperand SetDiff(SimplifiedOperand a, SimplifiedOperand b)
        {
            Console.WriteLine("What is the set difference of " + a.Symbol + " and " + b.Symbol + "?");
            var result = Console.ReadLine();
            return new SimplifiedOperand(result);
        }
    }
}