using System;
using System.Collections.Generic;
using System.Linq;

namespace Lexer
{
    internal delegate SimplifiedOperand Operation(SimplifiedOperand leftOperand, SimplifiedOperand rightOperand);

    /// <summary>
    ///   Contains methods for simplifying expressions.
    /// </summary>
    internal static class Simplification
    {
        private static readonly Dictionary<char, Operation> Operations;

        static Simplification()
        {
            Operations = new Dictionary<char, Operation>();
            Operations[','] = OperationImplementations.Union;
            Operations[':'] = OperationImplementations.Intersection;
            Operations['!'] = OperationImplementations.SetDiff;
            // TODO: Add more operations here.
        }

        public static SimplifiedOperand Simplify(Operand operand)
        {
            if (operand is SimplifiedOperand)
                return (SimplifiedOperand) operand;

            if (operand is Expression)
            {
                var leftOperand = Simplify(((Expression) operand).LeftOperand);
                var rightOperand = Simplify(((Expression) operand).RightOperand);

                return Operations[((Expression) operand).Operator](leftOperand, rightOperand);
            }

            throw new Exception(
                "Something magical has happened! Hope you're running this with your IDE's debugger, broseph.");
        }

        /// <summary>
        ///   Checks whether the operator is valid, and returns it in an appropriate type.
        /// </summary>
        /// <param name = "rawOperator">A 1 character long string.</param>
        /// <returns>rawOperator[0] if it is a valid operator symbol</returns>
        internal static char ParseOperator(string rawOperator)
        {
            if (rawOperator.Length != 1)
            {
                throw new Exception("Invalid operator: " + rawOperator + " (not one character long)");
            }

            var result = rawOperator[0];

            if (Operations.Keys.Contains(result)) return result;
            
            throw new Exception("Invalid operator: " + rawOperator + " (doesn't match any expected operator symbols)");
        }
    }
}