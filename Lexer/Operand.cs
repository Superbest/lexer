using System;

namespace Lexer
{
    /// <summary>
    ///   Represents any expression which can be the argument of an operator.
    /// </summary>
    public abstract class Operand
    {
        public static Operand ParseOperand(string expression)
        {
            var trimmed = TrimParens(expression);

            var isElementary = IsSimplified(trimmed);

            if (isElementary)
            {
                return new Expression(trimmed);
            }
            return new SimplifiedOperand(trimmed);
        }

        /// <summary>
        ///   If the entire input is enclosed in parantheses, removes those parens. Also trims whitespace.
        /// </summary>
        /// <param name = "input"></param>
        /// <returns>input with the outermost, redundant parens removed, if any.</returns>
        private static string TrimParens(string input)
        {
            if (HasRedundantParens(input))
            {
                var partiallyTrimmed = input.Substring(1, input.Length - 2);

                return TrimParens(partiallyTrimmed);
            }
            return input;
        }

        /// <summary>
        ///   Determines whether expression is in the simplest possible state, or if it can be reduced further.
        /// </summary>
        /// <param name = "expression"></param>
        /// <returns>True if cannot be reduced.</returns>
        [Obsolete("Add real logic here.")]
        private static bool IsSimplified(string expression)
        {
            bool isElementary;

            while (true)
            {
                Console.WriteLine("Can " + expression + " be simplified any further? y/n");

                var response = Console.ReadLine();
                if (response == "y")
                {
                    isElementary = true;
                    break;
                }
                if (response == "n")
                {
                    isElementary = false;
                    break;
                }

                Console.WriteLine("What part of y or n did you not understand?");
            }

            return isElementary;
        }

        /// <summary>
        ///   Determines whether there is a pair of parentheses (redundantly) enclosing the entire expression.
        /// </summary>
        /// <param name = "expression"></param>
        /// <returns></returns>
        [Obsolete("Add real logic here.")]
        private static bool HasRedundantParens(string expression)
        {
            while (true)
            {
                Console.WriteLine("Is " + expression + " enclosed by outermost redundant prantheses? y/n");

                var response = Console.ReadLine();
                if (response == "y") return true;
                if (response == "n") return false;

                Console.WriteLine("Can't you read? y or n, please.");
            }
        }
    }
}