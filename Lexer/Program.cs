﻿using System;

namespace Lexer
{
    /// <summary>
    ///   A proof-of-concept for the Stack Overflow answer http://stackoverflow.com/a/12184912/1042555
    /// </summary>
    internal class Program
    {
        private static void Main()
        {
            // Original input given by Ash
            const string originalInput = "(AK,AQ,(A2:*h*h)):*s*s!AsQs,(JJ:*s*h)";

            // A shorter input for debugging
            const string shortInput = "AK,AQ,(A2:*h*h)";

            while (true)
            {
                Console.WriteLine("Enter an expression to simplify, or enter:" +
                                  "\n\t\"0\" to exit, " +
                                  "\n\t\"1\" to use \"" + originalInput + "\"" +
                                  "\n\t\"2\" to use \"" + shortInput + "\"");
                var input = Console.ReadLine();

                if (input == "0") break;

                SimplifiedOperand result;
                switch (input)
                {
                    case "1":
                        result = EvaluateInput(originalInput);
                        break;
                    case "2":
                        result = EvaluateInput(shortInput);
                        break;
                    default:
                        result = EvaluateInput(input);
                        break;
                }

                Console.WriteLine("The result is: " + result.Symbol + "\n");
            }
        }

        /// <summary>
        ///   This function can be called by other code to utilize the parser.
        /// </summary>
        /// <param name = "input">Must conform to the syntax described here: http://www.propokertools.com/simulations/generic_syntax</param>
        /// <returns>The most simple representation of the input</returns>
        private static SimplifiedOperand EvaluateInput(string input)
        {
            var parsedExpression = Operand.ParseOperand(input);

            var result = Simplification.Simplify(parsedExpression);

            return result;
        }
    }
}